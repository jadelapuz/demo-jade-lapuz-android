package com.jade.reqres.model.register

data class RegisterBody(
	val email: String,
	val password: String)
