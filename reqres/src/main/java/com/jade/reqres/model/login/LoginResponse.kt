package com.jade.reqres.model.login

data class LoginResponse(
	val token: String?,
	val error: String?)
