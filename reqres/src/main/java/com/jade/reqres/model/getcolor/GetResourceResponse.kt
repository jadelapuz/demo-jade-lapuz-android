package com.jade.reqres.model.getcolor

import com.jade.reqres.model.Resource

data class GetResourceResponse(
	val data: Resource)
