package com.jade.reqres.model.register

data class RegisterResponse(
	val token: String?,
	val error: String?)
