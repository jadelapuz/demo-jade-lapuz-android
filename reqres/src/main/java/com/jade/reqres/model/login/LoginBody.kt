package com.jade.reqres.model.login

data class LoginBody(
	val email: String,
	val password: String)
