package com.jade.demotemplate.domain.ext

import com.jade.demotemplate.domain.entity.Color
import com.jade.reqres.model.Resource

/**
 * Map Resource into Color
 */
fun Resource.toColorEntity() = Color(
    id = id,
    name = name,
    hex = color,
    pantoneValue = pantoneValue
)
