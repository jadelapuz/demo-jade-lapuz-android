package com.jade.demotemplate._di

import com.jade.reqres.ReqresDatasource
import com.jade.litunyi.helpers.WebService
import org.koin.dsl.module

/**
 * Remote Web Service datasource
 */
val remoteDatasourceModule = module {
	// ReqresDatasource
	single {
		WebService.create<ReqresDatasource>(
			ReqresDatasource.baseUrl,
			Pair("Content-type", "application/json")
		)
	}
}
