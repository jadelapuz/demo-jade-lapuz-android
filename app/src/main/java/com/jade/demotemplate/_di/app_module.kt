package com.jade.demotemplate._di

import com.jade.litunyi.coroutines.AppSchedulerProvider
import com.jade.demotemplate.domain.ColorsRepository
import com.jade.demotemplate.domain.ColorsRepositoryImpl
import com.jade.litunyi.coroutines.SchedulerProvider
import com.jade.demotemplate.view.MainViewModel
import com.jade.demotemplate.view.colors.ColorsViewModel
import com.jade.demotemplate.view.details.DetailsViewModel
import com.jade.demotemplate.view.login.LoginViewModel
import com.jade.demotemplate.view.register.RegisterViewModel
import com.jade.demotemplate.view.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/** App Components for Dependency Injection **/

val mainModule = module {
    viewModel { MainViewModel() }
    viewModel { SplashViewModel(get(), get()) }
    viewModel { RegisterViewModel(get(), get()) }
    viewModel { LoginViewModel(get(), get()) }
    viewModel { ColorsViewModel(get(), get()) }
    viewModel { (colorId: Int) ->
        DetailsViewModel(colorId, get(), get())
    }

    // ColorsRepository
    single<ColorsRepository>(createdAtStart = true) { ColorsRepositoryImpl(get()) }

    // SchedulerProvider
    single<SchedulerProvider>(createdAtStart = true) { AppSchedulerProvider() }
}

val appModules = listOf(mainModule, remoteDatasourceModule)