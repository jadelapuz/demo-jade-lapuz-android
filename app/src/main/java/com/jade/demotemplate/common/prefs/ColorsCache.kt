package com.jade.demotemplate.common.prefs

import com.chibatching.kotpref.KotprefModel
import com.chibatching.kotpref.gsonpref.gsonPref
import com.jade.demotemplate.domain.entity.Color

/**
 * App cache
 */
object ColorsCache : KotprefModel() {
    var token by stringPref()
    var colors by gsonPref(mutableListOf<Color>())
}