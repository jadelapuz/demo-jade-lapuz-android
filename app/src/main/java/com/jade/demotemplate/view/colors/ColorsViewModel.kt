package com.jade.demotemplate.view.colors

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jade.litunyi.coroutines.RxViewModel
import com.jade.litunyi.coroutines.SchedulerProvider
import com.jade.litunyi.coroutines.SingleLiveEvent
import com.jade.litunyi.coroutines.with
import com.jade.demotemplate.domain.ColorsRepository
import com.jade.demotemplate.domain.entity.Color
import io.reactivex.rxkotlin.subscribeBy

class ColorsViewModel(
    private val colorsRepository: ColorsRepository,
    private val schedulerProvider: SchedulerProvider
) : RxViewModel() {

    private val _state = MutableLiveData<ColorsState>()
    val state: LiveData<ColorsState> get() = _state

    private val _event = SingleLiveEvent<ColorsEvent>()
    val event: LiveData<ColorsEvent> get() = _event

    fun setTitleLastClicked(id: Int) {
        val title = _state.value?.colors?.first { it.id == id }?.name ?: "Colors"
        _state.value = _state.value?.copy(title = title)
    }

    /** Triggers **/

    fun loadColors() {
        _event.value = ColorsEvent.Pending
        launch {
            colorsRepository.getColors().with(schedulerProvider)
                .subscribeBy(
                    onSuccess = { colors ->
                        _event.value = ColorsEvent.LoadColorsSuccess
                        _state.value = _state.value?.copy(colors = colors) ?: ColorsState(colors = colors)
                    },
                    onError = { error -> _event.value = ColorsEvent.LoadColorsFailed(error) })
        }
    }

}

data class ColorsState(
    val title: String? = "Colors",
    val colors: List<Color> = emptyList())

sealed class ColorsEvent {
    object Pending : ColorsEvent()
    object LoadColorsSuccess : ColorsEvent()
    data class LoadColorsFailed(val error: Throwable) : ColorsEvent()
}
