package com.jade.demotemplate.view.login

import androidx.lifecycle.LiveData
import com.jade.litunyi.coroutines.RxViewModel
import com.jade.litunyi.coroutines.SchedulerProvider
import com.jade.litunyi.coroutines.SingleLiveEvent
import com.jade.litunyi.coroutines.with
import com.jade.demotemplate.domain.ColorsRepository
import io.reactivex.rxkotlin.subscribeBy

class LoginViewModel(
    private val colorsRepository: ColorsRepository,
    private val schedulerProvider: SchedulerProvider
) : RxViewModel() {

    private val _event = SingleLiveEvent<LoginEvent>()
    val event: LiveData<LoginEvent> get() = _event

    fun login(email: String, password: String) {
        _event.value = LoginEvent.Pending
        launch {
            colorsRepository.login(email, password).with(schedulerProvider)
                .subscribeBy(
                    onComplete = { _event.value = LoginEvent.LoginSuccess },
                    onError = { error -> _event.value = LoginEvent.LoginFailed(error) })
        }
    }

}

sealed class LoginEvent {
    object Pending : LoginEvent()
    object LoginSuccess : LoginEvent()
    data class LoginFailed(val error: Throwable) : LoginEvent()
}